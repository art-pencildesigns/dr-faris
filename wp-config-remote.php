<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pencilpr_drfaris' );

/** MySQL database username */
define( 'DB_USER', 'pencilpr_drfaris' );

/** MySQL database password */
define( 'DB_PASSWORD', '2$O.=ozLLp5e' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&/Uo>BNa|F!B)40opY5CcHrd82+|nO<LrOIEFeyD?BaL_];A X*%dP:P|]Dn&qM?' );
define( 'SECURE_AUTH_KEY',  'ch2m0+)n,c|4>/VOZqwG+%%>![y2g&Rq=Mzx20^&O./3koWU*^W<n:YIYk%]pn*=' );
define( 'LOGGED_IN_KEY',    'NyUjGAJ!-4;/KaRrI;V:~%FxQ>MQ3cY1wlHLL6.Bba1aeHcV~*V]8[A.v ufYazc' );
define( 'NONCE_KEY',        'C1%vX*btsOJ3+W:|f>AFdG9ePiGTp7/@o#}L~@G]sbTA>*D6:.l.}Y7Uegj%zsQ|' );
define( 'AUTH_SALT',        'i=%$.q5qo6 7d2P7l00!E5Z1/=lr(:|]SJabeWi@H4&*NMOT+Xd~265(|fnF!L5)' );
define( 'SECURE_AUTH_SALT', '-JL&vm4I`WJb3jaWN#{{?.1xla[,BnjqYI}q:7?L>%!pRpTR)jg($+J4sY:MB;Z,' );
define( 'LOGGED_IN_SALT',   'Hg;4b({Q|aK9UK&%c);jwnVX!-k^ aq&?)<<gmvODpiY_1S+kvEC|YJByZrB4<JG' );
define( 'NONCE_SALT',       '`WQgf1M;OoiJKVqA1o~*|^<f$vT.)zD!RX}dB@MX_;[9 _4@MQsa5uF?Kt%|-^/M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('ALLOW_UNFILTERED_UPLOADS', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
