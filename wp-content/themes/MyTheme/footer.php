<!-- Footer -->
<div class="footer">
    <div class="jumbotron">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-sm-5 col-12">
                    <div>
                        <h4 class="text-white"><?= get_field('footer_title', tt(210, 1157)) ?></h4>
                        <p class="pt-4 textPage text-white"><?= get_field('footer_content', tt(210, 1157)) ?></p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="pt-5 fRight">
                        <button class="btn btn-outline-light  mr-2" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>
                        <a class="btn btn-outline-light" href="contact-us"> <?= get_field('booking', tt(210, 1157))['button_text_2']; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container pt-2 pb-5">
            <div class="row justify-content-around">
                <div class="col-lg-4 col-md-4 col-sm-3 col-12 pb-4">
                    <img src="<?= get_template_directory_uri() ?>/images/2.png" class="pt-4 imgFoot d-block mx-auto">
                </div>

                <div class="col pt-2">
                    <h6>Company</h6>
                    <?php foreach (get_field('col-1', tt(210, 1157)) as $key => $field) : ?>
                        <p><a href="<?= $field['link'] ?>"> <?= $field['link_text'] ?> </a></p>
                    <?php endforeach; ?>
                </div>
                <div class="col pt-2">
                    <h6>Services</h6>
                    <?php foreach (get_field('col-2', tt(210, 1157)) as $key => $field) : ?>
                        <p><a href="<?= $field['link'] ?>"> <?= $field['link_text'] ?> </a></p>
                    <?php endforeach; ?>
                </div>
                <div class="col pt-2">
                    <h6>Social</h6>
                    <?php if (get_field('social', 531)['twitter']) { ?>
                        <a href="<?= get_field('social', 531)['twitter']; ?>"><i class="fab fa-twitter-square fa-lg text-primary"></i> </a>
                    <?php } ?>
                    <?php if (get_field('social', 531)['facebook']) { ?>
                        <a href="<?= get_field('social', 531)['facebook']; ?>"><i class="fab fa-facebook-square fa-lg text-primary"></i> </a>
                    <?php } ?>
                    <?php if (get_field('social', 531)['youtube']) { ?>
                        <a href="<?= get_field('social', 531)['youtube']; ?>"><i class="fab fa-youtube-square fa-lg text-danger"></i> </a>
                    <?php } ?>
                    <?php if (get_field('social', 531)['instagram']) { ?>
                        <a href="<?= get_field('social', 531)['instagram']; ?>"><i class="fab fa-instagram fa-lg text-danger"></i> </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <hr class="hr2 p-0">
    <div>
        <div class="container firstHead">
            <p class="text-center copyRight"> Copyright &copy; 2020 Dr Faris Medical Center - Designed @
                <a class="linkPencil" href="https://pencil-designs.com/">Pencil Designs</a>
            </p>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="bookAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content p-2">
            <div class="modal-header pb-0 border-bottom-0">
                <h6 class="text-primary txtMons">RESERVE</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-3">
                <h4 class="colorB txtLibre"><?= get_field('booking', tt(210, 1157))['button_text'] ?></h4>
                <p class="pMe"><?= get_field('booking', tt(210, 1157))['description']; ?></p>

                <div class="pt-3">

                    <div class="wpforms-container wpforms-container-full" id="wpforms-606">
                        <form id="wpforms-form-606" class="wpforms-validate wpforms-form form" data-formid="606" method="post" enctype="multipart/form-data" action="/faris-wordpress/fertility/"><noscript class="wpforms-error-noscript">Please enable JavaScript in your browser to complete this form.</noscript>
                            <div class="wpforms-field-container row">
                                <div id="wpforms-606-field_1-container" class="wpforms-field wpforms-field-text form-group my-1 col-6" data-field-id="1"><label class="wpforms-field-label" for="wpforms-606-field_1">Name <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-606-field_1" class="wpforms-field-large wpforms-field-required form-control" name="wpforms[fields][1]" required=""></div>
                                <div id="wpforms-606-field_2-container" class="wpforms-field wpforms-field-phone form-group my-1 col-6" data-field-id="2"><label class="wpforms-field-label" for="wpforms-606-field_2">Phone <span class="wpforms-required-label">*</span></label><input type="tel" id="wpforms-606-field_2" class="wpforms-field-large wpforms-field-required form-control wpforms-smart-phone-field" data-rule-smart-phone-field="true" name="wpforms[fields][2]" required=""></div>
                                <div id="wpforms-606-field_4-container" class="wpforms-field wpforms-field-select form-group my-1 col-12" data-field-id="4"><label class="wpforms-field-label" for="wpforms-606-field_4">Select An Inquiry <span class="wpforms-required-label">*</span></label><select id="wpforms-606-field_4" class="wpforms-field-large wpforms-field-required form-control" name="wpforms[fields][4]" required="required">
                                        <?php foreach (get_field('booking', tt(210, 1157))['select_inquiry'] as $key => $val) : ?>
                                            <option value="<?= $val["inquiry"] ?>"><?= $val["inquiry"] ?></option>
                                        <?php endforeach; ?>
                                    </select></div>
                            </div>
                            <div id="wpforms-606-field_5-container" class="wpforms-field wpforms-field-textarea form-group my-3" data-field-id="5"><label class="wpforms-field-label" for="wpforms-606-field_5">Paragraph Text <span class="wpforms-required-label">*</span></label><textarea id="wpforms-606-field_5" class="wpforms-field-small wpforms-field-required form-control" name="wpforms[fields][5]" required=""></textarea></div>
                            <div class="wpforms-field wpforms-field-hp d-none"><label for="wpforms-606-field-hp" class="wpforms-field-label">Phone</label><input type="text" name="wpforms[hp]" id="wpforms-606-field-hp" class="wpforms-field-medium"></div>
                            <div class="wpforms-submit-container form-group mt-5"><input type="hidden" name="wpforms[id]" value="606"><input type="hidden" name="wpforms[author]" value="0"><input type="hidden" name="wpforms[post_id]" value="325"><button type="submit" name="wpforms[submit]" class="wpforms-submit btn btnBook btn-block mt-3" id="wpforms-submit-606" value="wpforms-submit" aria-live="assertive" data-alt-text="Sending..." data-submit-text="Submit">Send</button></div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="<?= get_template_directory_uri() ?>/package/js/swiper.min.js"></script>
<script src="<?= get_template_directory_uri() ?>/js/file.js"></script>
<script>
    $(".accordion h5>.fa-angle-down, .accordion h5>.fa-angle-up").on("click", function() {
        if ($(this).hasClass("fa-angle-down")) {
            $(this).removeClass("fa-angle-down").addClass("fa-angle-up")
        } else $(this).removeClass("fa-angle-up").addClass("fa-angle-down")
    })
    window.onscroll = function() {
        myFunction()
    };

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }


    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 15,
        freeMode: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            // when window width is <= 499px
            320: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
            },
            700: {
                slidesPerView: 2,
                spaceBetweenSlides: 50
            },
            // when window width is <= 999px
            999: {
                slidesPerView: 3,
                spaceBetweenSlides: 15
            }
        }
    });
</script>




<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v7.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your customer chat code -->
<div class="fb-customerchat" attribution=setup_tool page_id="102701601377134" logged_in_greeting="Hello, How Can We Help You ?" logged_out_greeting="Hello, How Can We Help You ?">
</div>

</body>

</html>