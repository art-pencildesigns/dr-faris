<?php


function remove_from_page()
{
    remove_post_type_support('page', 'editor');
    remove_post_type_support('page', 'author');
    remove_post_type_support('page', 'excerpt');
    remove_post_type_support('page', 'comments');
    remove_post_type_support('page', 'page-attributes');
    remove_post_type_support('page', 'post-formats');
    remove_post_type_support('page', 'slug');
    add_theme_support('post-thumbnails');
    // set_post_thumbnail_size(50, 50);
    // add_image_size('single-post-thumbnail', 590, 180);
}
add_action('admin_init', 'remove_from_page');



function post_remove()
{
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'post_remove');



function my_custom_mime_types($mimes)
{
    // New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    $mimes['doc'] = 'application/msword';

    // Optional. Remove a mime type.
    unset($mimes['exe']);
    return $mimes;
}
add_filter('upload_mimes', 'my_custom_mime_types');
