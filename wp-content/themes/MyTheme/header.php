<?php
function tt($enStr, $arStr)
{
    return (pll_current_language() == "en") ? $enStr : $arStr;
}

global $wp;
$currentLink = home_url($wp->request);

global $polylang;
$currentPostId = get_the_ID();
// $translationIds = $polylang->model->get_translations('post', get_the_ID());
if (pll_current_language() == "en") $samePageInAnotherLangURL = get_permalink(pll_get_post($currentPostId, 'ar'));
else $samePageInAnotherLangURL = get_permalink(pll_get_post($currentPostId, 'en'));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/package/css/swiper.min.css">
    <link rel="icon" href="<?= get_template_directory_uri() ?>/images/logo.png">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/style.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/style_facilities.css">
    <?php if (pll_current_language() == "ar") { ?>
        <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/styleAr.css">
    <?php } ?>
    <title> DR.Faris | <?= the_title() ?> </title>
</head>

<body>

    <!-- Header -->
    <header class="pt-2">
        <div class="container firstHead pt-1">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <p class="textHeader firHead pt-1"><img class="pr-2" src="<?= get_template_directory_uri() ?>/images/map.svg"> <?= tt(get_field('location', 531), get_field('location_ar', 531)); ?> </p>
                </div>
                <div class="col-lg-2 col-md-6 col-7">
                    <p class="textHeader textMedia pt-1"><img class="pr-2" src="<?= get_template_directory_uri() ?>/images/mail.svg"><a href="mailto:<?= get_field('email', 531); ?>"> <?= get_field('email', 531); ?> </a></p>
                </div>
                <div class="row col-lg-4 col-md-6 col-5">
                    <?php foreach (get_field('phone_numbers', 531) as $key => $field) : ?>
                        <p class="textHeader telephone textMedia col-lg-6 pt-1"><img class="pr-2" src="<?= get_template_directory_uri() ?>/images/call.svg">
                            <a href="tel:<?= str_replace(" ", "", $field["phone_number"]); ?>"><?= $field["phone_number"] ?></a>
                        </p>
                    <?php endforeach; ?>
                </div>
                <div class="col-lg-2 col-md-6 col-7">
                    <ul class="navbar-nav listaSocial ml-auto pr-2">
                        <li class="form-inline" dir="rtl">
                            <?php if (get_field('social', 531)['twitter']) { ?>
                                <a href="<?= get_field('social', 531)['twitter']; ?>" class="pr-2 social_1"><i class="fab fa-twitter-square fa-lg text-primary"></i></a> <?php } ?>
                            <?php if (get_field('social', 531)['facebook']) { ?>
                                <a href="<?= get_field('social', 531)['facebook']; ?>" class="pr-2 social_2"><i class="fab fa-facebook-square fa-lg"></i></a>
                            <?php } ?>
                            <?php if (get_field('social', 531)['youtube']) { ?>
                                <a href="<?= get_field('social', 531)['youtube']; ?>" class="pr-2 social_1"><i class="fab fa-youtube-square fa-lg text-danger"></i></a>
                            <?php } ?>
                            <?php if (get_field('social', 531)['instagram']) { ?>
                                <a href="<?= get_field('social', 531)['instagram']; ?>" class="social_2"><i class="fab fa-instagram fa-lg text-danger"></i></a>
                            <?php } ?>

                            <a href="<?= $samePageInAnotherLangURL ?>" class="btn bg-white btn-sm mr-3 mt-0 pt-0 d-none"><?= tt("AR", "EN") ?></a>

                        </li>
                    </ul>

                </div>
            </div class="col-lg-2 col-md-3 col-sm-3 col-5">
            <hr class="hr">
        </div>
    </header>

    <!-- Navbar -->
    <div class="container-fluid mt-0 bg-white" id="navbar">
        <nav class="navbar container navbar-expand-lg bg-white navbar-light bg-light py-1 px-0">

            <a class="navbar-brand" href="<?= get_field('home_page_link', 531); ?>">
                <img src="<?= get_template_directory_uri() ?>/images/7.png" height="47px" class="imgHeder" alt="Dr Faris">
                <a href="<?= $samePageInAnotherLangURL ?>" class="btn bg-white d-block d-sm-none btn-sm mr-3 mt-0 pt-1"><?= tt("AR", "EN") ?></a>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="aboutDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= tt("About Us", "عنا") ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="aboutDropdown">
                            <?php foreach (get_field('dropdown_links', 531) as $key => $field) : ?>
                                <a class="dropdown-item <?= (trim($field['link'], "/") == trim($currentLink, "/")) ? "active" : "" ?>" href="<?= $field['link'] ?>"> <?= tt($field['link_name'], $field['link_name_ar']) ?> </a>
                            <?php endforeach; ?>
                        </div>
                    </li>
                    <?php foreach (get_field('header_links', 531) as $key => $field) : ?>
                        <li class="nav-item">
                            <a class="nav-link head_link <?= (trim($field['link'], "/") == trim($currentLink, "/")) ? "active" : "" ?>" href="<?= $field['link'] ?>"> <?= tt($field['link_name'], $field['link_name_ar']) ?> </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

            </div>
        </nav>
    </div>