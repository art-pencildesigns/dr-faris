<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<?php
		if (have_posts()) {

			// Load posts loop.
			while (have_posts()) {
				the_post();
?>

<div class="container news">
        <div class="row justify-content-around">
            <div class="col-md-5">
                <h4 class="txtLibre colorB">In the news</h4>
                <p class="pMe py-2">Here are some of the best fertility hospitals that trust and endorse Dr.Faris’ Medical Center</p>
            </div>
            <div class="col-md-6 team">
                <ul class="nav nav-pills justify-content-end mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link px-4 active" id="pills-all-tab" data-toggle="pill" href="#pills-all" 
                        role="tab" aria-controls="pills-all" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-news-tab" data-toggle="pill" href="#pills-news" 
                        role="tab" aria-controls="pills-news" aria-selected="false">News (5)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-videos-tab" data-toggle="pill" href="#pills-videos" 
                        role="tab" aria-controls="pills-videos" aria-selected="false">Videos (4)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-events-tab" data-toggle="pill" href="#pills-events" 
                        role="tab" aria-controls="pills-events" aria-selected="false">Events (3)</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane container pt-3 fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">News fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane container pt-3 fade" id="pills-news" role="tabpanel" aria-labelledby="pills-news-tab">
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">News fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">News fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane container pt-3 fade" id="pills-videos" role="tabpanel" aria-labelledby="pills-videos-tab">
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">News fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">News fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane container pt-3 fade" id="pills-events" role="tabpanel" aria-labelledby="pills-events-tab">
                <div class="row">
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">News fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="induction2 card mb-3">
                            <img src="images/rectangle.png" class="img-fluid">
                            <div class="card-body">
                                <h6 class="txtLibre colorB">New development for fertility</h6>
                                <p class="textPageSmall pt-3">If we decide to go the natural way, then we'll let you know at your last visit before term the signs to look out for that indicate you're going into labor. Your family of obstetricians, nurses, and staff at Dr. Faris Medical Center</p>
                                <p class="mb-0"><a href="#" class="txtLink text-primary">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

<?php
				
				echo the_title();
				get_template_part('template-parts/content/content');
			}
		} else {

			// If no content, include the "No posts found" template.
			get_template_part('template-parts/content/content', 'none');
		}
		?>

	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php
get_footer();
