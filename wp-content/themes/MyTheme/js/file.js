window.onscroll = function() {
  myFunction();
};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky");
  } else {
    navbar.classList.remove("sticky");
  }
}

let learnMore = function(elm) {
  $($(elm))
    .parent()
    .find(".three_dots_text")
    .click();
};

$(document).ready(function() {
  $(".three_dots_text").click(evt => {
    $(evt.currentTarget).toggleClass("three_dots");
    console.log(evt.currentTarget);
    let less = $(evt.currentTarget)
      .parent()
      .find(".seeLess")
      .toggle("slow");
    console.log(less);
    let more = $(evt.currentTarget)
      .parent()
      .find(".seeMore")
      .toggle("slow");
    evt.preventDefault();
    evt.stopPropagation();
  });

  setTimeout(() => {
    $(".three_dots_text").each((ind, elm) => {
      if ($(elm).text().length > 200)
        $(elm).parent()
          .append(`<p class='text-center text-primary hand moreIndicator seeMore' onclick='learnMore(this)'>
                More
            </p>`);
    });
  }, 300);
});

$(document).ready(function() {
  $(window).on("scroll", function() {
    if ($(window).scrollTop() > 50) {
      $("#navbar").css(
        "box-shadow",
        " 0 8px 39px 7px rgba(255, 255, 255, 0.07), 0 10px 13px -6px rgba(0, 0, 0, 0.15)"
      );
    } else {
      $("#navbar").css("box-shadow", "none");
    }
  });

  // $("#aboutDropdown").hover(
  //   evt => {
  //     evt.preventDefault();
  //     $(evt.currentTarget).click();
  //   },
  //   evt => {
  //     evt.preventDefault();
  //     $(evt.currentTarget).click();
  //   }
  // );
  // $("#aboutDropdown").mouseleave(evt => {
  //   evt.preventDefault();
  //   $(evt.currentTarget).click();
  // });

  $(".dropdown").hover(function() {
    var dropdownMenu = $(this).children(".dropdown-menu");
    // console.log(dropdownMenu);
    dropdownMenu.toggleClass("show");
    dropdownMenu.parent().toggleClass("show open");
    // if (dropdownMenu.is(":visible")) { }
  });
});
