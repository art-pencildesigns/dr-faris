<?php get_header(); ?>

<!-- 1: -->
<div class="jumbotron mt-0 jumbotronAboutUs">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-lg-7">
                <div>
                    <h3 class="text-white"><?= get_field('title'); ?> </h3>
                    <p class="pt-3 text-white"><?= nl2br(get_field('description')); ?></p>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="secondPage">
                    <div class="card cardDr mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-5">
                                <img src="<?= get_field('dr_image'); ?>" class="img-fluid">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body text-left"> <?= get_field('doctor_text'); ?> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Section -->



<!-- 2: -->
<div class="jumbotron py-3 bg-white">
    <div class="container">
        <!-- <h2 class="py-3 colorB">About</h2>
        <img src="<?= get_field('image'); ?>" class="img-fluid pt-3 pb-2">
        <div class="row pt-5">
            <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                <h2 class="aboutTeam colorB pl-2"><?= get_field('title'); ?></h2>
                <p class="ptext pt-3 pl-2"><?= get_field('content'); ?></p>
            </div>
        </div>
        <hr class="hr2 p-0 mt-4"> -->
        <!-- End Section -->

        <!-- <hr class="hr2 p-0 mt-4"> -->
        <div class="mt-5"> </div>
        <h2 class="aboutTeam colorB pl-2"><?= tt("Our Legacy and Timeline", "تاريخنا و قصتنا من البداية") ?></h2>




        <!-- 3: -->
        <div class="container aboutSection pt-5 pb-3">
            <div class="row pb-2">
                <div class="col-lg-7 col-md-6 col-12 ">
                    <h4 class="mb-3 font-weight-bold colorB pt-3"><?= get_field('title_1'); ?></h4>
                    <p class="ptext pt-2"><?= get_field('content_1'); ?></p>
                </div>
                <div class="col-lg-5 col-md-6 col-12 text-right ">
                    <img src="<?= get_field('image_1'); ?>" class="img-fluid float-right">
                </div>
            </div>
            <p class="ptext"><?= nl2br(get_field('text_block_1')); ?></p>
            <hr class="hr2 p-0 mt-5">
            <div class="row my-4">
                <div class="col-lg-5 col-md-6 col-12 text-right ">
                    <img src="<?= get_field('image_2'); ?>" class="img-fluid float-left">
                </div>
                <div class="col-lg-7 col-md-6 col-12 pt-1">
                    <h4 class="mb-3 pHead pt-3 font-weight-bold colorB"><?= get_field('title_2'); ?></h4>
                    <p class="ptext"><?= get_field('content_2'); ?></p>
                </div>
            </div>
            <p class="ptext"><?= nl2br(get_field('text_block_2')); ?></p>
            <hr class="hr2 p-0 mt-5">


            <div class="row py-4">
                <div class="col-lg-7 col-md-6 col-12 ">
                    <h4 class="mb-3 pHead font-weight-bold colorB"><?= get_field('title_3'); ?></h4>
                    <p class="ptext pt-2"><?= get_field('content_3'); ?></p>
                </div>
                <div class="col-lg-5 col-md-6 col-12 text-right ">
                    <img src="<?= get_field('image_3'); ?>" class="img-fluid float-right">
                </div>
            </div>
            <p class="ptext"><?= nl2br(get_field('text_block_3')); ?></p>
            <!-- <hr class="hr2 p-0 mt-5"> -->


            <!-- <?php foreach (get_field("faq_section") as $key => $field) : ?>
                <div class="accordion border-0 md-accordion" id="accordionExEx" role="tablist"
                    aria-multiselectable="true">
                    <div class="card border-0 firstUploadCollapse">

                        <div class="card-header bg-white pl-0 pt-3 border-0" role="tab">
                            <a data-toggle="collapse" data-parent="#accordionExEx" href=".elm-<?= $key ?>"
                                aria-expanded="true" aria-controls="elm-<?= $key ?>">
                                <h5 class="mb-2 px-0 border-0 text-dark">
                                    <?= $field["question"] ?>
                                    <i class="float-right fas fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <div class="collapse borderd border-top-0 elm-<?= $key ?>" role="tabpanel"
                            aria-labelledby="headingOne1" data-parent="#accordionExEx">
                            <div class="card-body">
                                <div class="pl-0 ml-0">
                                    <p class="ptext"><?= $field["answer"] ?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="hr2 p-0">
                </div>
                <?php endforeach ?> -->

        </div>
    </div>
</div>




<?php get_footer() ?>