<?php get_header(); ?>


<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')['mini_headline']; ?></h2>
            <h1 class="pt-2"><?= get_field('section_1')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')['description']; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<div class="container facily pt-5 pb-3">
    <div class="row">
        <div class="col-lg-9 col-sm-9">
            <div class="treatments">
                <h4 class="colorB pb-2 txtLibre"><?= get_field('section_1')['headline_2']; ?></h4>
                <p><?= get_field('section_1')['description_2']; ?></p>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3">
            <div>
                <button class="btn btn-primary py-3 text-center btn-block" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>
            </div>
        </div>
    </div>

    <hr>

    <div class="container facility" id="facility_page">
        <ul class="nav nav-pills" role="tablist">
            <?php foreach (get_field("section_2") as $key => $field) : ?>
                <li class="nav-item">
                    <a class="nav-link text-center <?= $key == 0 ? 'active' : "" ?>" id="tab-<?= $key ?>" data-toggle="pill" href="#elm-<?= $key ?>" role="tab" aria-controls="elm-<?= $key ?>" aria-selected="<?= ($key == 0) ? "true" : "false" ?>">
                        <!-- <img src="<?= $field["symbol"] ?>" class="img-fluid d-block mx-auto pt-1 AlignSymbol"> -->
                        <?= file_get_contents($field["symbol"]) ?>

                        <p class="text-center pt-0"><?= $field["title"] ?></p>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <hr style="margin-top: -0.2rem;">


        <div class="tab-content" id="pills-tabContent">
            <?php foreach (get_field("section_2") as $key => $field) : ?>
                <div class="tab-pane pt-2 container fade <?php if ($key == 0) echo "show active" ?>" id="elm-<?= $key ?>" role="tabpanel" aria-labelledby="tab-<?= $key ?>">
                    <div class="treatments">
                        <h2 class="pt-2 pb-4"><?= $field["title"] ?></h2>
                        <p><?= $field["description"] ?></p>
                    </div>
                    <?php if (!empty($field["images"])) { ?>
                        <!-- <div class="swiper-container sw1 container gallery-top">
                            <div class="swiper-wrapper">
                                <?php foreach ($field["images"] as $key => $image) : ?>
                                    <div class="swiper-slide" style="background-image:url(<?= $image['image'] ?>)"></div>
                                <?php endforeach ?>
                            </div>
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                </div>
                <div class="swiper-container container gallery-thumbs">
                    <div class="swiper-wrapper">
                        <?php foreach ($field["images"] as $key => $image) : ?>
                            <div class="swiper-slide" style="background-image:url(<?= $image['image'] ?>)"></div>
                        <?php endforeach ?>
                    </div>
                </div> -->

                        <div id="carouselExampleControls-<?= $key ?>" class="carousel newSlide slide pb-3 pt-2" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php foreach ($field["images"] as $key1 => $field1) : ?>
                                    <div class="carousel-item <?= ($key1 == 0) ? 'active' : '' ?>">
                                        <img class="d-block w-100" src="<?= $field1["image"] ?>" alt="First slide">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?php if (count($field["images"]) > 1) : ?>
                                <a class="carousel-control-prev" href="#carouselExampleControls-<?= $key ?>" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls-<?= $key ?>" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            <?php endif ?>
                        </div>

                    <?php } ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<?php if (!empty(get_field('section_3')['left_section'])) : ?>

    <div class="container mapSec p-5">
        <hr />
        <div class="row">
            <div class="col-md-8">
                <div class="induction2 p-4">
                    <div class="row">
                        <div class="col-md-7">
                            <?= get_field('section_3')['left_section']; ?>
                        </div>
                        <div class="col-md-5">
                            <p class="textPageSmall py-2"><?= get_field('section_3')['right_text']; ?></p>
                            <button class="btn btn-outline-primary btn-block py-3" data-toggle="modal" data-target="#bookAppointment"><?= get_field('section_3')['button_text']; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="induction2 mapAr p-4">
                    <div class="mapouter">
                        <div class="gmap_canvas">

                            <?= get_field('section_3')['map_iframe']; ?>


                        </div>
                    </div><?= get_field('section_3')['under_map']; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>




<div class="container">
    <!-- <hr> -->
    <!-- <?php foreach (get_field("faq_section") as $key => $field) : ?> -->
    <!-- <div class="accordion border-0 md-accordion" id="accordionExEx" role="tablist"
                    aria-multiselectable="true">
                    <div class="card border-0 firstUploadCollapse">

                        <div class="card-header bg-white pl-0 pt-3 border-0" role="tab">
                            <a data-toggle="collapse" data-parent="#accordionExEx" href=".elm-<?= $key ?>"
                                aria-expanded="true" aria-controls="elm-<?= $key ?>">
                                <h5 class="mb-2 px-0 border-0 text-dark">
                                    <?= $field["question"] ?>
                                    <i class="float-right fas fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <div class="collapse borderd border-top-0 elm-<?= $key ?>" role="tabpanel"
                            aria-labelledby="headingOne1" data-parent="#accordionExEx">
                            <div class="card-body">
                                <div class="pl-0 ml-0">
                                    <p class="ptext"><?= $field["answer"] ?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="hr2 p-0">
                </div> -->
    <!-- <?php endforeach ?> -->

</div>

<?php get_footer() ?>