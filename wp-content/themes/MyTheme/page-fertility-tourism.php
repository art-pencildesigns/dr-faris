<?php get_header(); ?>

<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')['mini_headline']; ?></h2>
            <h1><?= get_field('section_1')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')['description']; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<div class="container direcAr pt-5 pb-3">

    <div class="row">
        <div class="col-lg-9 col-sm-9">
            <div class="treatments">
                <h4 class="pt-2 colorB pb-2 txtLibre"><?= get_field('section_1')['headline_2']; ?></h4>
                <p><?= get_field('section_1')['description2']; ?></p>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3">
            <div>
                <button class="btn btn-primary py-3 text-center btn-block" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>
            </div>
        </div>
    </div>
    <div id="tourism">
    <hr class="hr2 p-0 my-4">
    <h4 class="txtLibre colorB pt-5 pb-2"><?= get_field('section_2')['headline']; ?></h4>
    <p class="pMe pb-3"><?= get_field('section_2')['description']; ?></p>
</div>
    <div class="container py-3">
        <div class="row">
            <?php foreach (get_field('section_2')['boxes'] as $key => $field) : ?>
                <a class="col-md-4 col-sm-6 col-12 notLink" href="<?= $field['link_url'] ?>#<?= $field['section_id'] ?>">
                    <div class="my-2 induction p-3">
                        <img src="<?= $field["symbol"] ?>" class="img-fluid">
                        <h6 class="py-2"><?= $field["title"] ?></h6>
                        <p class="panel-body textPageSmall text-left"><?= $field["content"] ?></p>
                    </div>
                </a>
            <?php endforeach ?>

            <div class="col-md-8 pt-5">
                <div class="jumbotron vertical-center newJumb py-5 mt-5">
                    <div class="container">
                        <div class="row align-middle">
                            <div class="col-lg-8 col-12">
                                <div>
                                    <h5 class="text-white"><?= get_field('blue_box', tt(224, 1158))['title']; ?></h5>
                                    <p class="textPageSmall text-white pt-4"><?= get_field('blue_box', tt(224, 1158))['description']; ?></p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-12">
                                <div class="pt-5 fRight">
                                    <button class="btn btn-outline-light btn-block mr-2" data-toggle="modal" data-target="#bookAppointment"><?= get_field('blue_box', tt(224, 1158))['button_text']; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h4 class="txtLibre colorB"><?= get_field('section_3')['title']; ?></h4>
        <p class="pMe pt-3"><?= get_field('section_3')['description']; ?></p>
        </p>
        <div class="row">

            <?php foreach (get_field('section_3')['boxes'] as $key => $field) : ?>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="my-2 induction p-3">
                        <img src="<?= $field["symbol"] ?>" class="img-fluid">
                        <h6 class="py-2"><?= $field["title"] ?></h6>
                        <p class="panel-body textPageSmall text-left"><?= $field["content"] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    <hr>

    <div class="container pt-5">
        <h4 class="txtLibre colorB"><?= get_field('section_5')['title']; ?></h4>
        <p class="pMe"><?= get_field('section_5')['description']; ?></p>
        <div class="row py-3">
            <?php foreach (get_field('section_5')['boxes'] as $key => $field) : ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="my-2 induction text-center p-3">
                        <h2 class="txtLibre"><span class="badge bdg badge-primary py-2 px-3"><?= $key + 1 ?></span></h2>
                        <h6 class="py-2 colorB"><?= $field["title"] ?></h6>
                        <p class="panel-body textPageSmall"><?= $field["content"] ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>


        <h4 class="txtLibre colorB pt-4"><?= get_field('section_6')['title']; ?></h4>

        <!-- <div class="row pt-3">
                <div class="col-md-8">
                    <img src="<?= get_field('section_6')['image_1']; ?>" class="img-fluid">
                </div>
                <div class="col-md-4">
                    <img src="<?= get_field('section_6')['image_2']; ?>" class="img-fluid">
                </div>
            </div> -->

        <!-- slider old -->
        <!-- <div class="swiper-container mt-5 sw1 container gallery-top">
            <div class="swiper-wrapper">
                <?php foreach (get_field('section_4') as $key => $field) : ?>
                    <div class="swiper-slide" style="background-image:url(<?= $field["img"] ?>)"></div>
                <?php endforeach; ?>
            </div>

            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
        <div class="swiper-container container gallery-thumbs">
            <div class="swiper-wrapper">
                <?php foreach (get_field('section_4') as $key => $field) : ?>
                    <div class="swiper-slide" style="background-image:url(<?= $field["img"] ?>)"></div>
                <?php endforeach; ?>
            </div>
        </div> -->

        <!-- New Slider -->
        <div id="carouselExampleControls" class="carousel newSlide slide pb-3 pt-2" data-ride="carousel">
            <div class="carousel-inner">
                <?php foreach (get_field('section_4') as $key => $field) : ?>
                    <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                        <img class="d-block w-100" src="<?= $field["img"] ?>" alt="First slide">
                    </div>
                <?php endforeach; ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>


        <div class="row pb-4">
            <div class="col-lg-12">
                <p class="pMe"><?= get_field('section_6')['description']; ?></p>
                <hr>
            </div>
        </div>

                
        <div class="container sponser">
            <div class="row justify-content-around pt-5 pb-5">
                <div class="col-md-5">
                    <h3 class="txtLibre colorB pb-3"><?= get_field('affiliation', tt(263, 840))['title']; ?></h3>
                    <p class="ptext"><?= get_field('affiliation', tt(263, 840))['description']; ?></p>
                </div>
                <?php foreach (get_field('affiliation', tt(263, 840))['images'] as $key => $field) : ?>
                    <div class="col-md-2 col-3 pt-3">
                        <a href="#"><img src="<?= $field["image"] ?>" class="img-fluid mx-auto d-block"></a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>


    </div>
    <!-- <hr class="hr2 p-0 mt-1"> -->


</div>


<?php get_footer() ?>