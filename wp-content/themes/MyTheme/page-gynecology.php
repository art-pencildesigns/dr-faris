<?php get_header(); ?>

<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')['mini_headline']; ?></h2>
            <h1 class="pt-2"><?= get_field('section_1')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')['description']; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<div class="container gynecologySection pt-5 pb-3">
    <div class="row">
        <div class="col-lg-9 col-sm-9">
            <div class="treatments">
                <h4 class="colorB pb-2 txtLibre"><?= get_field('section_1')['headline_2']; ?></h4>
                <p><?= get_field('section_1')['description_2']; ?></p>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3">
            <div>
                <button class="btn btn-primary py-3 text-center btn-block" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>
            </div>
        </div>
    </div>


<div id="gynecology">
    <hr class="hr2 p-0 my-4">
    <div class="gynecology mt-5" style="background: url('<?= get_field('image_2') ?>') top center;"></div>
    <img src="<?= get_field('image_2'); ?>" class="img-fluid d-md-none d-lg-none d-sm-block">
    <h4 class="mb-3 pHead pt-5 pb-3"><?= get_field('title_2'); ?></h4>
    <p class="ptext"><?= nl2br(get_field('content_2')); ?></p>
</div>



    <!-- <div class="row pt-3">
            <?php foreach (get_field('boxes') as $key => $field) : ?>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="my-2 induction2 gyneco p-3">
                        <img src="<?= $field["image"] ?>" class="img-fluid">
                        <h6 class="py-2"><?= $field["title"] ?></h6>
                        <p class="ptext"><?= $field["content"] ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div> -->

    <div class="card-columns cardGynecology pt-3">
        <?php foreach (get_field('boxes') as $key => $field) : ?>
            <div class="card border-0">
                <div class="my-2 induction2 card-body p-3">
                    <img src="<?= $field["image"] ?>" class="img-fluid">
                    <h6 class="py-2"><?= $field["title"] ?></h6>
                    <p class="ptext"><?= $field["content"] ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <h4 class="mb-3 pHead pt-5"><?= get_field('section_4_title'); ?></h4>
    <p class="ptext"><?= nl2br(get_field('section_4_description')); ?></p>

    <div class="row justify-content-around pt-4">
        <?php foreach (get_field('boxes_2') as $key => $field) : ?>
            <div class="col-md-4 col-sm-6 col-12">
                <div class="p-2 induction">
                    <img src="<?= $field["image"] ?>" class="img-fluid mx-auto pb-2 d-block">
                    <h6 class="txtH font-weight-bold text-center py-3"><?= $field["title"] ?></h6>
                    <p class="panel-body textPage text-center three_dots_text"><?= $field["content"] ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- <div class="jumbotron py-5 mt-5">
            <div class="row justify-content-around">
                <div class="col-sm-8 col-12">
                    <div>
                        <h5 class="text-white"><?= get_field('blue_box', tt(224, 1158))['title']; ?></h5>
                        <p class="textPageSmall text-white pt-4"><?= get_field('blue_box', tt(224, 1158))['description']; ?></p>
                    </div>
                </div>
                <div class="col-sm-4 col-12">
                    <div class="pt-5 fRight">
                        <button class="btn btn-outline-light btn-block mr-2" data-toggle="modal" data-target="#bookAppointment"><?= get_field('blue_box', tt(224, 1158))['button_text']; ?></button>
                    </div>
                </div>
            </div>
        </div> -->


    <?php foreach (get_field("faq_section") as $key => $field) : ?>
        <!-- <div class="accordion border-0 md-accordion" id="accordionExEx" role="tablist"
                    aria-multiselectable="true">
                    <div class="card border-0 firstUploadCollapse">

                        <div class="card-header bg-white pl-0 pt-3 border-0" role="tab">
                            <a data-toggle="collapse" data-parent="#accordionExEx" href=".elm-<?= $key ?>"
                                aria-expanded="true" aria-controls="elm-<?= $key ?>">
                                <h5 class="mb-2 px-0 border-0 text-dark">
                                    <?= $field["question"] ?>
                                    <i class="float-right fas fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <div class="collapse borderd border-top-0 elm-<?= $key ?>" role="tabpanel"
                            aria-labelledby="headingOne1" data-parent="#accordionExEx">
                            <div class="card-body">
                                <div class="pl-0 ml-0">
                                    <p class="ptext"><?= $field["answer"] ?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="hr2 p-0">
                </div> -->
    <?php endforeach ?>



    <div class="my-5">
        <h4 class="txtLibre font-weight-bold colorB pt-5"><?= get_field('treatments_section')['headline']; ?></h4>
        <p class="colorB"><?= get_field('treatments_section')['description']; ?></p>

        <div class="row">
            <?php foreach (get_field('treatments_section')["boxes"] as $key => $field) : ?>
                <div class="col-sm-6">
                    <div class="induction2 card mb-3">
                        <img src="<?= $field['image'] ?>" class="img-fluid">
                        <div class="card-body pb-1">
                            <h6 class="txtLibre colorB font-weight-bold"><?= $field['title'] ?></h6>
                            <p class="panel-body textPageSmall text-left three_dots_text"><?= $field['content'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

</div>

<?php get_footer() ?>