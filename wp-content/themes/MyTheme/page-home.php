<?php get_header() ?>

<!-- Section One -->
<div class="container baby">
    <div class="row" dir="rtl">
        <div class="col-lg-6">
            <img src="<?= get_template_directory_uri() ?>/images/babyHead.png" class="imgWidth">
        </div>
        <div class="col-lg-6">
            <div class="introduction pt-2">
                <h5 class="pt-4"><?= get_field("first_text") ?></h5>
                <h2 class="pt-3"><?= get_field("second_text") ?></h2>
                <h6 class="pt-3 pb-3"><?= get_field("third_text") ?></h6>
                <button class="btn btnBook px-5 text-uppercase py-3" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>

            </div>
        </div>
    </div>
</div>
<!-- End Section -->






<!-- Section Two -->
<?php if (!empty(get_field('hyperlinks_boxes'))) : ?>
    <div class="container pt-5">
        <div class="card-group">
            <?php foreach (get_field('hyperlinks_boxes') as $key => $field) : ?>
                <a href="<?= $field['link'] ?>" class="card notLink">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= $field['image'] ?>">
                            <div class="media-body pl-3">
                                <h6 class="mt-0"><?= $field['title'] ?><i class="fa fa-chevron-circle-right pl-3 mt-2"></i></h6>
                                <p><?= $field['content'] ?></p>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
<!-- End Section -->



<?php if (!empty(get_field('section_3')["title"])) : ?>
    <!-- Section Three -->
    <div class="container ourInfo pt-4">
        <div class="row">
            <div class="col-lg-5">
                <div class="fertility mb-4">
                    <h2><?= get_field('section_3')["title"]; ?></h2>
                    <p class="textPageSmall py-2"><?= get_field('section_3')["description"]; ?></p>
                </div>
            </div>
            <div class="col-lg-1">
                <div class="vl"></div>
            </div>


            <?php foreach (get_field('section_3')["boxes"] as $key => $field) : ?>
                <div class="col-lg-2">
                    <div class="tretms">
                        <a href="<?= $field['link'] ?>" class="card notLink border-0 p-0 m-0">
                            <img src="<?= $field["image"] ?>" width="100%">
                            <div class="card-body p-2 m-0">
                                <h5 class="card-title font-weight-bold"><?= $field["title"] ?></h5>
                                <p class="card-text textPageSmall"><?= $field["description"] ?></p>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
    <hr class="hr2 p-0 mt-1">
    <!-- End Section -->
<?php endif ?>


<?php if (!empty(get_field('section_3')["boxes_2"])) : ?>
    <div class="container pt-4 pb-4 contentText">
        <div class="row justify-content-around">
            <?php foreach (get_field('section_3')["boxes_2"] as $key => $field) : ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12 pt-4">
                    <div class="text-center">
                        <img src="<?= $field["image"] ?>" height="80px" class="mx-auto d-block pb-4">
                        <h4><?= $field["title"] ?></h4>
                        <p class="textPageSmall"><?= $field["description"] ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <hr class="hr2 p-0 mt-1">
<?php endif ?>


<?php if (!empty(get_field('section_4')["tabs"])) : ?>
    <div class="container facility pt-3">
        <ul class="nav nav-pills justify-content-center" role="tablist">
            <?php foreach (get_field('section_4')["tabs"] as $key => $field) : ?>
                <li class="nav-item">
                    <a class="nav-link px-4  <?= ($key == 0) ? "active" : "" ?>" id="<?= $key ?>-tab" data-toggle="pill" href="#id-<?= $key ?>" role="tab" aria-controls="id-<?= $key ?>" aria-selected="false">
                        <p class="text-center pt-2"><?= $field["title"] ?></p>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <hr style="margin-top: -0.2rem;">

        <div class="tab-content" id="pills-tabContent">
            <?php foreach (get_field('section_4')["tabs"] as $key => $field) : ?>
                <div class="tab-pane pt-2  container fade <?= ($key == 0) ? "show active" : "" ?>" id="id-<?= $key ?>" role="tabpanel" aria-labelledby="<?= $key ?>-tab">
                    <div class="gynecology mt-3" style="background: url(<?= $field["image"] ?>);"></div>
                    <img src="<?= $field["image"] ?>" class="img-fluid d-md-none d-lg-none d-sm-block">
                    <div class="row pt-5">
                        <div class="col-md-12">
                            <h3 class="txtLibre colorB pt-2 pb-3"><?= $field["title"] ?></h3>
                            <p class="textPageSmall py-2"><?= $field["description"] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif ?>


<!-- Question -->
<?php if (!empty(get_field('section_4')["title"])) : ?>
    <div class="container how pt-5">
        <h3 class="txtLibre colorB py-4"><?= get_field('section_4')["title"]; ?></h3>
        <p class="textPageSmall"><?= get_field('section_4')["description"]; ?></p>
    </div>
    <!-- Induction of Ovulation -->
    <div class="container py-3">
        <div class="row">
            <?php foreach (get_field('section_4')["boxes"] as $key => $field) : ?>
                <a class="col-md-4 col-sm-6 col-12 notLink" href="<?= $field['link'] ?>#<?= $field['section_id'] ?>">
                    <div class="my-2 induction p-3">
                        <img src="<?= $field["image"] ?>" class="img-fluid">
                        <h6 class="py-2"><?= $field["title"] ?></h6>
                        <p class="panel-body textPageSmall text-left"><?= $field["description"] ?></p>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif ?>




<div class="container sponser">
    <div class="row justify-content-around pt-5 pb-5">
        <div class="col-md-5">
            <h3 class="txtLibre colorB pb-3"><?= get_field('affiliation', tt(263, 840))['title']; ?></h3>
            <p class="ptext"><?= get_field('affiliation', tt(263, 840))['description']; ?></p>
        </div>
        <?php foreach (get_field('affiliation', tt(263, 840))['images'] as $key => $field) : ?>
            <div class="col-md-2 col-3 pt-3">
                <a href="#"><img src="<?= $field["image"] ?>" class="img-fluid mx-auto d-block"></a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<hr class="hr2 p-0 mt-1">


<?php if (!empty(get_field('section_4')["testimonials_section"])) : ?>
    <hr class="hr2 p-0 mt-1">
    <!-- Slider -->
    <?php if (!empty(get_field('section_4')["testimonials_section"])) : ?>
        <section class="slideSection">
            <div id="demo" class="carousel slide" data-ride="carousel">
                <div class="container">
                    <ol class="carousel-indicators">
                        <?php foreach (get_field('section_4')["testimonials_section"] as $key => $field) : ?>
                            <li data-target="#demo" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>
                        <?php endforeach ?>
                    </ol>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <?php foreach (get_field('section_4')["testimonials_section"] as $key => $field) : ?>
                            <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                                <div class="row justify-content-around" dir="rtl">
                                    <div class="col-lg-5 col-md-4 col-6">
                                        <img src="<?= $field["person_image"] ?>" class="float-right">
                                    </div>
                                    <div class="col-lg-5 col-md-8 col-6 pt-3 secHide" dir="ltr">
                                        <h2><i class="fa fa-quote-left"></i></h2>
                                        <h2><?= $field["testimony"] ?> <i class="fa fa-quote-right"></i></h2>
                                        <h3><?= $field["person_name"] ?></h3>
                                        <h4><?= $field["person_title"] ?></h4>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </section>
        <!--End of  slider -->
    <?php endif; ?>
<?php endif; ?>

<?php
$our_news = get_posts([
    'post_type' => 'our_news',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'order'    => 'DESC'
]);
if (!empty($our_news)) :
?>
    <div class="container">
        <hr>
        <h4 class="txtLibre font-weight-bold colorB pt-4 mb-3"><?= tt("Our News", "أخبارنا") ?>
            <a href="news" class="btn btnSeeAll float-right px-4"> <?= tt("See All", "عرض الكل") ?> </a>
        </h4>
        <div class="swiper-container container pb-5 pt-3"><br>
            <div class="swiper-wrapper">


                <?php foreach ($our_news as $key => $field) : setup_postdata($field); ?>
                    <div class="swiper-slide">
                        <div class="row py-3 justify-content-around">
                            <div class="col-lg-12 pt-3">
                                <a class="induction2 card mb-3 notLink" href="<?= $field->guid ?>">
                                    <img src="<?= get_the_post_thumbnail_url($field->ID) ?>" class="imgCardNews">
                                    <div class="card-body news_article">
                                        <h6 class="txtLibre colorB font-weight-bold"><?= $field->post_title ?></h6>
                                        <p class="panel-body textPageSmall text-left"><?= substr(strip_tags(get_the_content()), 0, 100) ?> ...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach;
                ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-button-next"><img src="<?= get_template_directory_uri() ?>/images/right.svg"></div>
            <div class="swiper-button-prev"><img src="<?= get_template_directory_uri() ?>/images/left.svg"></div>
        </div><br>
    </div>
<?php endif ?>



<?php get_footer() ?>