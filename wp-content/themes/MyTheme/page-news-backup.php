<?php get_header() ?>

<?php
$our_news = get_posts([
    'post_type' => 'our_news',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'order'    => 'DESC'
]);
if (!empty($our_news)) :
?>
    <div class="container tretms pt-5 my-5">
        <h4 class="txtLibre font-weight-bold colorB pt-5 mb-3">Our News</h4>

        <div class="row">
            <?php foreach ($our_news as $key => $field) :
                // $image_url = wp_get_attachment_image_url($field->id);
            ?>
                <div class="col-sm-6">
                    <a class="induction2 card mb-3 notLink" href="<?= $field->guid ?>">
                        <?php echo get_the_post_thumbnail($field->ID); ?>
                        <div class="card-body pb-1">
                            <h6 class="txtLibre colorB font-weight-bold"><?= $field->post_title ?></h6>
                            <p class="panel-body textPageSmall text-left three_dots_text"><?= substr($field->post_content, 0, 200) ?></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>



<?php get_footer() ?>