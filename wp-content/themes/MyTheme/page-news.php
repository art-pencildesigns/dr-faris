<?php get_header() ?>

<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')["headline"]; ?></h2>
            <h1><?= get_field('section_1')["main_headline"]; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')["headline_description"]; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<!-- <div class="container facily pb-3">
    <div class="row">
        <div class="col-lg-6 col-sm-6">
            <div class="treatments">
                <h2 class="pt-4 pb-2"><?= get_field('section_1')['main_headline']; ?></h2>
                <p><?= get_field('section_1')['headline_description_2']; ?></p>
            </div>
        </div>
    </div>
    <hr>
</div> -->



<?php
$our_news = get_posts([
    'post_type' => 'our_news',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'order'    => 'DESC'
]);
if (!empty($our_news)) :
?>

    <div class="container tretms my-5">
        <h4 class="txtLibre font-weight-bold colorB pb-3">Our News</h4>

        <div class="row">
            <?php foreach ($our_news as $key => $field) :
                setup_postdata($field);
                // $image_url = wp_get_attachment_image_url($field->id);
                // $output = get_the_content();
                // echo '<pre>';
                // var_export($output);
                // die;
            ?>
                <div class="col-md-4 col-sm-6">
                    <a class="induction2 card mb-3 notLink" href="<?= $field->guid ?>">
                        <img src="<?= get_the_post_thumbnail_url($field->ID) ?>" class="imgCardNews">
                        <div class="card-body news_article">
                            <h6 class="txtLibre colorB font-weight-bold"><?= $field->post_title ?></h6>
                            <p class="panel-body textPageSmall text-left"><?= substr(strip_tags(get_the_content()), 0, 100) ?> ...</p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
<?php endif; ?>



<?php get_footer() ?>