<?php get_header(); ?>




<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')['mini_headline']; ?></h2>
            <h1 class="pt-2"><?= get_field('section_1')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')['description']; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<div class="container tretms pt-5 pb-3">
    <div class="row">
        <div class="col-lg-9 col-sm-9">
            <div class="treatments">
                <h4 class="colorB pb-2 txtLibre"><?= get_field('section_1')['headline_2']; ?></h4>
                <p><?= get_field('section_1')['description_2']; ?></p>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3">
            <div>
                <button class="btn btn-primary py-3 text-center btn-block" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>
            </div>
        </div>
    </div>

    <?php foreach (get_field('boxes_1') as $key => $field) : ?>
        <hr class="hr2 p-0 my-4">
        <h4 class="txtLibre colorB py-4"><?= $field["title"] ?></h4>
        <p class="pMe pb-3"><?= $field["content"] ?></p>
    <?php endforeach; ?>



    <div class="container pb-3 mt-3">
        <ul class="nav nav-tabs tabLink tabNewStyle" id="myTab" role="tablist">
            <?php foreach (get_field('boxes_2') as $key => $field) : ?>
                <li class="nav-item">
                    <a class="nav-link <?= ($key == 0) ? 'active' : '' ?>" id="<?= $key ?>-tab" data-toggle="tab" href="#idtabs1-<?= $key ?>" role="tab" aria-controls="id-<?= $key ?>" aria-selected="true"><?= $field["title"] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content pb-5" id="myTabContent">
            <?php foreach (get_field('boxes_2') as $key => $field) : ?>
                <div class="tab-pane pt-3 container tabFil fade <?= ($key == 0) ? 'show active' : '' ?>" id="idtabs1-<?= $key ?>" role="tabpanel" aria-labelledby="evaluations-tab">
                    <img src="<?= $field["image"] ?>" class="img-fluid">
                    <h2 class="txtLibre colorB pt-4"><b><?= $field["title"] ?></b></h2>
                    <div class="row">
                        <div class="col-lg-6"> <?= $field["content"] ?> </div>
                        <div class="col-lg-6"> <?= $field["right_content"] ?> </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>


    <!-- <h1>hello</h1> -->
    <div class="container py-3">
        <!-- <div class="row">
            <?php foreach (get_field('boxes_2') as $key => $field) : ?>
                <div class="col-sm-6 col-12">
                    <div class="my-2 induction p-3">
                        <img src="<?= $field['image'] ?>" class="img-fluid">
                        <h4 class="txtLibre py-2"><?= $field['title'] ?></h4>
                        <p class="pMe pb-3"><?= $field['content'] ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if (!empty(get_field('images_repeater_1'))) {
                foreach (get_field('images_repeater_1') as $key => $field) : ?>
                    <div class="col-sm pt-4 pb-2">
                        <img src="<?= $field['image'] ?>" class="img-fluid">
                    </div>
            <?php endforeach;
            } ?>
        </div> -->


        <div class="row">
            <div class="col-md-8">
                <h4 class="txtLibre colorB py-4"><?= get_field('boxes_3')['title']; ?></h4>
                <div><?= get_field('boxes_3')['content']; ?></div>
            </div>
            <div class="col-md-4">
                <img src="<?= get_field('boxes_3')['pregnancy_timeline_image']; ?>" class="imgSide">
                <button class="btn btn-block btn-primary text-uppercase py-3 px-5" data-toggle="modal" data-target="#timeWeeks"><?= get_field('boxes_3')['button_text']; ?></button>
            </div>
        </div>
    </div>

    <!-- <div class="container weeks pt-3">
        <div class="induction2 py-5">
            <h5 class="txtLibre pl-5 colorB font-weight-bold">Weeks</h5>
            <ul class="nav nav-pills justify-content-center py-5" role="tablist">
                <?php foreach (get_field('boxes_3')['weeks'] as $key => $field) : ?>
                    <li class="nav-item">
                        <a class="nav-link <?= ($key == 0) ? 'active' : '' ?>" data-toggle="pill" href="#id-<?= $key ?>"><?= $field['week_number'] ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <?php foreach (get_field('boxes_3')['weeks'] as $key => $field) : ?>
                    <div class="tab-pane container <?= ($key == 0) ? 'show active' : '' ?>" id="id-<?= $key ?>">
                        <div class="row justify-content-around">
                            <div class="col-md-5 pt-2">
                                <h6 class="colorB txtLibre">Week <?= $field['week_number'] ?> :</h6>
                                <p class="pMe"><?= $field['content'] ?></p>
                            </div>
                            <div class="col-md-5 pt-3">
                                <img src="<?= $field['image'] ?>" class="img-fluid imgWeek">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php if (!empty(get_field('images_repeater_2'))) {
            foreach (get_field('images_repeater_2') as $key => $field) : ?>
                <div class="col-sm pt-4 pb-2">
                    <img src="<?= $field['image'] ?>" class="img-fluid">
                </div>
        <?php endforeach;
        } ?>
    </div> -->

    <hr>

    <div class="container tretms">
        <h4 class="txtLibre font-weight-bold colorB pt-5"><?= get_field('delivery_section')['headline']; ?></h4>
        <p class="colorB"><?= get_field('delivery_section')['description']; ?></p>

        <div class="row">
            <?php foreach (get_field('delivery_section')["boxes"] as $key => $field) : ?>
                <div class="col-sm-6">
                    <div class="induction2 card mb-3">
                        <img src="<?= $field['image'] ?>" class="img-fluid">
                        <div class="card-body pb-1">
                            <h6 class="txtLibre colorB font-weight-bold"><?= $field['title'] ?></h6>
                            <p class="panel-body textPageSmall text-left three_dots_text"><?= $field['content'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <hr>


        <h4 class="txtLibre font-weight-bold colorB pt-5"><?= get_field('post_delivery_section')['headline']; ?></h4>
        <p class="colorB"><?= get_field('post_delivery_section')['description']; ?></p>

        <div class="row">
            <?php foreach (get_field('post_delivery_section')["boxes"] as $key => $field) : ?>
                <div class="col-sm-6 col-12">
                    <div class="induction2 card mb-3">
                        <img src="<?= $field['image'] ?>" class="img-fluid">
                        <div class="card-body pb-1">
                            <h6 class="txtLibre colorB font-weight-bold"><?= $field['title'] ?></h6>
                            <p class="pMe pb-3 three_dots_text"><?= $field['content'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?php if (!empty(get_field('images_repeater_3'))) {
            foreach (get_field('images_repeater_3') as $key => $field) : ?>
                <div class="col-sm pt-4 pb-2">
                    <img src="<?= $field['image'] ?>" class="img-fluid">
                </div>
        <?php endforeach;
        } ?>
    </div>
    <hr>


    <div class="container tretms">
        <h4 class="txtLibre font-weight-bold colorB pt-5"><?= get_field('abnormalities_of_pregnancy_section')['headline']; ?></h4>
        <p class="colorB"><?= get_field('abnormalities_of_pregnancy_section')['description']; ?></p>

        <div class="row">
            <?php foreach (get_field('abnormalities_of_pregnancy_section')["boxes"] as $key => $field) : ?>
                <div class="col-sm-6 col-12">
                    <div class="induction2 card mb-3">
                        <img src="<?= $field['image'] ?>" class="img-fluid">
                        <div class="card-body pb-1">
                            <h6 class="txtLibre colorB font-weight-bold"><?= $field['title'] ?></h6>
                            <p class="pMe pb-3 three_dots_text"><?= $field['content'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <hr class="hr2 p-0 mt-1">
</div>


<div class="modal fade" id="timeWeeks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content p-2">
            <div class="modal-header pl-4 pb-0 border-bottom-0">
                <h6 class="txtLibre pt-4 colorB font-weight-bold"><?= get_field('boxes_3')['button_text']; ?></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container weeks">
                    <div>
                        <?= get_field('boxes_3')['timeline_text']; ?>
                        <!-- <p class="pMe pl-5 pb-3">Your obstetrician (that is what we call doctors who handle pregnancies and births) will begin counting your pregnancy from the 1st day of your last period, which marks the beginning of a monthly cycle. You actually got pregnant two weeks after that (in the middle of your cycle), so the first two weeks are a bit empty (we just use that time to standardize our measurements).</p> -->
                        <h5 class="txtLibre pl-5 colorB font-weight-bold"><?= tt("Weeks", "الأسابيع") ?></h5>
                        <ul class="nav nav-pills justify-content-center py-5" role="tablist">
                            <?php foreach (get_field('boxes_3')['weeks'] as $key => $field) : ?>
                                <li class="nav-item">
                                    <a class="nav-link <?= ($key == 0) ? 'active' : '' ?>" data-toggle="pill" href="#id-<?= $key ?>"><?= $field['week_number'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (get_field('boxes_3')['weeks'] as $key => $field) : ?>
                                <div class="tab-pane container <?= ($key == 0) ? 'show active' : '' ?>" id="id-<?= $key ?>">
                                    <div class="row justify-content-around">
                                        <div class="col-md-5 pt-2">
                                            <h6 class="colorB txtLibre"><?= tt("Week", "الأسبوع") ?> <?= $field['week_number'] ?> :</h6>
                                            <p class="pMe"><?= $field['content'] ?></p>
                                        </div>
                                        <div class="col-md-5 pt-3">
                                            <img src="<?= $field['image'] ?>" class="img-fluid imgWeek rounded-lg">
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php if (!empty(get_field('images_repeater_2'))) {
                        foreach (get_field('images_repeater_2') as $key => $field) : ?>
                            <div class="col-sm pt-4 pb-2">
                                <img src="<?= $field['image'] ?>" class="img-fluid">
                            </div>
                    <?php endforeach;
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer() ?>