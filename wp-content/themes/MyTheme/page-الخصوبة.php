<?php get_header(); ?>




<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')['mini_headline']; ?></h2>
            <h1 class="pt-2"><?= get_field('section_1')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')['description']; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<div class="container tretms pt-5 pb-3">
    <div class="row">
        <div class="col-lg-9 col-sm-9">
            <div class="treatments">
                <h4 class="pt-2 colorB pb-2 txtLibre"><?= get_field('section_1')['headline_2']; ?></h4>
                <p><?= get_field('section_1')['description2']; ?></p>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3">
            <div>
                <button class="btn btn-primary py-3 text-center btn-block" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210, 1157))['button_text']; ?></button>
            </div>
        </div>
    </div>
    <hr class="hr2 p-0 my-4">

    <div class="row">
        <div class="col-md-8">
            <h4 class="txtLibre colorB py-4"><?= get_field('section_2')['headline']; ?></h4>
            <p class="pMe pb-3"><?= get_field('section_2')['description']; ?></p>
        </div>
        <div class="col-md-4">
            <img src="<?= get_field('section_2')['how_babies_are_made_image']; ?>" class="imgSide">
            <button class="btn btn-block btn-primary text-uppercase py-3 px-5" data-toggle="modal" data-target="#timeWeeks"><?= get_field('section_2')['how_babies_are_made_button_text']; ?></button>
        </div>
    </div>


    <ul class="nav nav-tabs tabLink" id="myTab" role="tablist">
        <?php foreach (get_field('section_2')['boxes'] as $key => $field) : ?>
            <li class="nav-item">
                <a class="nav-link <?= ($key == 0) ? 'active' : '' ?>" id="<?= $key ?>-tab" data-toggle="tab" href="#id-<?= $key ?>" role="tab" aria-controls="id-<?= $key ?>" aria-selected="true"><?= $field["title"] ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content pb-5" id="myTabContent">
        <?php foreach (get_field('section_2')['boxes'] as $key => $field) : ?>
            <div class="tab-pane pt-3 container tabFil fade <?= ($key == 0) ? 'show active' : '' ?>" id="id-<?= $key ?>" role="tabpanel" aria-labelledby="evaluations-tab">
                <img src="<?= $field["image"] ?>" class="img-fluid">
                <h5 class="txtLibre colorB pt-4"><?= $field["title"] ?></h5>
                <div class="row py-3">
                    <div class="col-md-7">
                        <p class="pMe"><?= $field["left_content"] ?></p>
                    </div>
                    <div class="col-md-5">
                        <p class="pMe"><?= $field["right_content"] ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>


    <h4 class="txtLibre colorB py-3"><?= get_field('section_3')['headline']; ?></h4>
    <p class="pMe pb-3"><?= get_field('section_3')['description']; ?></p>
    <div class="container py-3">
        <div class="row">
            <?php foreach (get_field('section_3')['boxes'] as $key => $field) : ?>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="my-2 induction px-3 pt-3 pb-1">
                        <img src="<?= $field["image"] ?>" class="img-fluid">
                        <h6 class="py-2"><?= $field["title"] ?></h6>
                        <p class="panel-body textPageSmall text-left">
                            <?= $field["content"] ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="col-md-8 pt-5">
                <div class="jumbotron py-5 mt-5">
                    <div class="row justify-content-around">
                        <div class="col-sm-8 col-12">
                            <div>
                                <h5 class="text-white"><?= get_field('blue_box', tt(224, 1158))['title']; ?></h5>
                                <p class="textPageSmall text-white pt-4"><?= get_field('blue_box', tt(224, 1158))['description']; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-12">
                            <div class="pt-5 fRight">
                                <button class="btn btn-outline-light btn-block mr-2" data-toggle="modal" data-target="#bookAppointment"><?= get_field('blue_box', tt(224, 1158))['button_text']; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- <div class="container pt-5">
        <h4 class="txtLibre colorB"><?= get_field('section_4')['headline']; ?></h4>
        <div class="row">
            <div class="col-md-8">
                <p class="pMe"><?= get_field('section_4')['description']; ?></p>
                <div class="row">
                    <div class="col-md-5">
                        <div class="induction p-2">
                            <h6><img src="<?= get_field('section_4')['image_1']; ?>" class="img-fluid"> <?= get_field('section_4')['text_1']; ?></h6>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="induction p-2">
                            <h6><img src="<?= get_field('section_4')['image_2']; ?>" class="img-fluid"> <?= get_field('section_4')['text_2']; ?></h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <img src="<?= get_field('section_4')['image_3']; ?>" class="img-fluid">
            </div>
        </div>
    </div> -->
    <hr>






    <?php if (!empty(get_field('section_8')["headline"])) : ?>
        <div class="container how pt-5">
            <h3 class="txtLibre colorB py-4"><?= get_field('section_8')["headline"]; ?></h3>
            <p class="textPageSmall"><?= get_field('section_8')["description"]; ?></p>
        </div>
        <!-- Induction of Ovulation -->
        <div class="container py-3">
            <div class="row">
                <?php foreach (get_field('section_8')["boxes"] as $key => $field) : ?>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="my-2 induction p-3">
                            <img src="<?= $field["image"] ?>" class="img-fluid">
                            <h6 class="py-2"><?= $field["title"] ?></h6>
                            <p class="panel-body textPageSmall text-left"><?= $field["content"] ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif ?>





    <hr>


    <?php if (!empty(get_field('section_6')["title"])) : ?>
        <div class="container how pt-5">
            <h3 class="txtLibre colorB py-4"><?= get_field('section_6')["title"]; ?></h3>
            <p class="textPageSmall"><?= get_field('section_6')['description']; ?></p>
        </div>
        <!-- Induction of Ovulation -->
        <div class="container py-3">
            <div class="row">
                <?php foreach (get_field('section_6')["boxes"] as $key => $field) : ?>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="my-2 induction p-3">
                            <img src="<?= $field["image"] ?>" class="img-fluid">
                            <h6 class="py-2"><?= $field["title"] ?></h6>
                            <p class="panel-body textPageSmall text-left"><?= $field["description"] ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <hr class="hr2 p-0 mt-1">
    <?php endif ?>







    <div class="container tretms">
        <div class="row py-4">
            <div class="col-lg-10">
                <h4 class="txtLibre colorB pt-1 pb-4"><?= get_field('section_5')['headline']; ?></h4>
                <p class="pMe"><?= get_field('section_5')['description']; ?></p>
            </div>
        </div>
        <div class="card-columns justify-content-start">
            <?php foreach (get_field('section_5')['boxes'] as $key => $field) : ?>
                <div class="induction2 card mb-3">
                    <img src="<?= $field["image"] ?>" class="img-fluid">
                    <div class="card-body">
                        <h6 class="txtLibre newTextCard colorB"><?= $field["title"] ?></h6>
                        <p class="panel-body textPageSmall text-left">
                            <?= $field["content"] ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>


        </div>
    </div>
    <hr class="hr2 p-0 mt-1">
</div>



<div class="modal fade" id="howBabies" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content p-2">
            <div class="modal-header pb-0 border-bottom-0">
                <h4 class="txtLibre colorB"><?= get_field('section_4')['headline']; ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="pMe"><?= get_field('section_4')['description']; ?></p>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="induction p-2">
                                        <h6><img src="<?= get_field('section_4')['image_1']; ?>" class="img-fluid"> <?= get_field('section_4')['text_1']; ?></h6>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="induction p-2">
                                        <h6><img src="<?= get_field('section_4')['image_2']; ?>" class="img-fluid"> <?= get_field('section_4')['text_2']; ?></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <img src="<?= get_field('section_4')['image_3']; ?>" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer() ?>