<?php get_header();
$avatar_link = get_template_directory_uri() . "/images/avatar.jpg";

?>
<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')['mini_headline']; ?></h2>
            <h1 class="pt-2"><?= get_field('section_1')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')['description']; ?></h4>
        </div>
    </div>
</div>



<!-- Info About Dr Faris -->
<div class="container pt-4 pb-0" id="teams-intro">
    <div class="row three_dots_text">
        <div class="col-md-9 colorB">
            <?= get_field('section_1')['description_2']; ?>
        </div>
        <div class="col-md-3">
            <img src="<?= get_field('section_1')['image']; ?>" class="img-fluid mx-auto d-block">
        </div>
    </div>
    <p class='text-center text-primary hand moreIndicator seeLess' onclick='learnMore(this)'> Less </p>
</div>
<hr class="hr2 p-0">


<!-- Team -->
<div class="container team">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link px-4 active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-Clinicans-tab" data-toggle="pill" href="#pills-Clinicans" role="tab" aria-controls="pills-Clinicans" aria-selected="false">Clinicans</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-Fertility-tab" data-toggle="pill" href="#pills-Fertility" role="tab" aria-controls="pills-Fertility" aria-selected="false">Fertility Lab</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-Nursing-tab" data-toggle="pill" href="#pills-Nursing" role="tab" aria-controls="pills-Nursing" aria-selected="false">Nursing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-Adminstration-tab" data-toggle="pill" href="#pills-Adminstration" role="tab" aria-controls="pills-Adminstration" aria-selected="false">Adminstration</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane container pt-3 fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
            <h3 class="txtLibre colorB pt-4 pb-5">Clinicians</h3>
            <div class="row">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Clinicians") continue;
                ?>
                    <div class=" col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr />

            <h3 class="txtLibre colorB">Administration</h3>
            <div class="row py-4">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Administration") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>

            <h3 class="txtLibre colorB py-5">Fertility Lab</h3>
            <div class="row pb-4">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Fertility Lab") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>

            <h3 class="txtLibre colorB py-5">Nursing</h3>
            <div class="row">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Nursing") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>



        </div>
        <hr>



        <div class="tab-pane pt-5 container fade" id="pills-Clinicans" role="tabpanel" aria-labelledby="pills-Clinicans-tab">
            <h3 class="txtLibre colorB">Clinicians</h3>
            <div class="row py-4">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Clinicians") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>
        </div>

        <div class="tab-pane pt-5 container fade" id="pills-Fertility" role="tabpanel" aria-labelledby="pills-Fertility-tab">
            <h3 class="txtLibre colorB">Fertility Lab</h3>
            <div class="row py-4">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Fertility Lab") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>
        </div>

        <div class="tab-pane pt-5 container fade" id="pills-Nursing" role="tabpanel" aria-labelledby="pills-Nursing-tab">
            <h3 class="txtLibre colorB">Nursing</h3>
            <div class="row">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Nursing") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>
        </div>
        <div class="tab-pane pt-5 container fade" id="pills-Adminstration" role="tabpanel" aria-labelledby="pills-Adminstration-tab">
            <h3 class="txtLibre colorB">Administration</h3>
            <div class="row py-4">
                <?php foreach (get_field('team_members') as $key => $field) {
                    if ($field["work_field"] != "Administration") continue;
                ?>
                    <div class="col-md-4 col-sm-6 col-12 pt-2">
                        <div class="willHover" data-person='<?= $key ?>'>
                            <img src="<?= $field['image'] ? $field['image'] : $avatar_link ?>" class="img-fluid imgCircle mx-auto d-block" alt="DOCTORS">
                            <h6 class="pt-2 hTeam text-center"><?= $field['name'] ?></h6>
                            <p class="text-center"><?= $field['job_title'] ?></p>
                            <p class="text-center ptext showMoreInModal"><?= $field['member_description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <hr>
        </div>

    </div>
</div>



<div class="modal fade" id="teamMember" tabindex="-1" role="dialog" aria-labelledby="teamMember" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content p-2">
            <div class="modal-header pb-0 border-bottom-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-3">
                <div class="row">
                    <div class="col-md-4 pt-2">
                        <img id="modal_img" src="" class="img-fluid">
                    </div>
                    <div class="col-md-8 pt-2">
                        <h3 class="colorB txtLibre" id="modal_name"></h3>
                        <h6 class="txtLibre" id="modal_title"></h6>
                        <!-- <ul class="list-group list-group-horizontal">
                                <li class="list-group-item border-0 pl-0">Recurrent Pregnancy Loss</li>
                                <li class="list-group-item border-0">Egg Freezing</li>
                                <li class="list-group-item border-0">Direct Ovulation Injection</li>
                            </ul> -->
                        <p class="textPageNos" id="modal_desc"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    let persons = <?= json_encode(get_field('team_members')) ?>;

    setTimeout(() => {
        $(".showModalButton").click(evt => {
            let key = $(evt.currentTarget).data("person");
            let person = persons[key];
            console.log(person);
            $("#modal_name").text(person.name);
            $("#modal_desc").html(person.member_description);
            $("#modal_title").text(person.job_title);
            $("#modal_img").attr("src", person.image || '<?= $avatar_link ?>');
        });
    }, 2000);

    setTimeout(() => {
        $(".showMoreInModal").each((ind, elm) => {
            if ($(elm).text().length > 170) $(elm).parent().append(`<p class="text-center"><a href="#" class="showModalButton" data-person='<?= $key ?>' data-toggle="modal" data-target="#teamMember">Learn More</a></p>`);
        });

        $(".willHover").click(evt => {
            if (!$(evt.currentTarget).find(".showModalButton").length) {
                return;
            }
            let key = $(evt.currentTarget).data("person");
            let person = persons[key];
            console.log(person);
            $("#modal_name").text(person.name);
            $("#modal_desc").html(person.member_description);
            $("#modal_title").text(person.job_title);
            $("#modal_img").attr("src", person.image || '<?= $avatar_link ?>');
            $("#teamMember").modal("show");
        })
    }, 1000);
</script>


<?php get_footer() ?>