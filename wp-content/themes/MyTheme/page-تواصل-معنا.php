<?php get_header(); ?>




<!-- section two babys -->
<div class="hero heroF" style="background: url('<?= get_field('section_11')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_11')['mini_headline']; ?></h2>
            <h1 class="pt-2"><?= get_field('section_11')['headline']; ?></h1>
            <h4 class="pt-3"><?= get_field('section_11')['description']; ?></h4>
        </div>
    </div>
</div>

<!-- Treatments -->
<!-- <div class="container pt-5 pb-3">

        <div class="row">
            <div class="col-lg-9 col-sm-9">
                <div class="treatments">
                    <h4 class="pt-2 colorB pb-2 txtLibre"><?= get_field('section_11')['headline_2']; ?></h4>
                    <p><?= get_field('section_11')['description2']; ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3">
                <div class="pt-4">
                    <button class="btn btn-primary py-3 text-center btn-block" data-toggle="modal" data-target="#bookAppointment"><?= get_field('booking', tt(210,1157))['button_text']; ?></button>
                </div>
            </div>
        </div>
        <hr class="hr2 p-0 my-4">

    </div> -->


<!-- Treatments -->
<div class="contactUs pt-5">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-md-5 txtLibre">
                <?= get_field('all_our_info'); ?>
                <p>
                    <a href="#" class="pr-2"><i class="fab fa-twitter-square fa-2x text-primary"></i></a>
                    <a href="#" class="pr-2"><i class="fab fa-facebook-square fa-2x"></i></a>
                    <a href="#" class="pr-2"><i class="fab fa-youtube-square fa-2x text-danger"></i></a>
                </p>
            </div>
            <div class="col-md-6">
                <?= get_field('contact_us_text'); ?>

                <div class="wpforms-container wpforms-container-full" id="wpforms-605">
                    <form id="wpforms-form-605" class="wpforms-validate wpforms-form form" data-formid="605" method="post" enctype="multipart/form-data" action="/faris-wordpress/contact-us/" _lpchecked="1"><noscript class="wpforms-error-noscript">Please enable JavaScript in your browser to complete this form.</noscript>
                        <div class="wpforms-field-container">
                            <div id="wpforms-605-field_1-container" class="wpforms-field wpforms-field-text my-4" data-field-id="1"><label class="d-none wpforms-field-label wpforms-label-hide" for="wpforms-605-field_1">Name <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-605-field_1" class="wpforms-field-large wpforms-field-required form-control form-control" name="wpforms[fields][1]" placeholder="الأسم" required="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;"></div>
                            <div id="wpforms-605-field_2-container" class="wpforms-field wpforms-field-email my-4" data-field-id="2"><label class="d-none wpforms-field-label wpforms-label-hide" for="wpforms-605-field_2">Email <span class="wpforms-required-label">*</span></label><input type="email" id="wpforms-605-field_2" class="wpforms-field-large wpforms-field-required form-control" name="wpforms[fields][2]" placeholder="الإيميل" required=""></div>
                            <div id="wpforms-605-field_5-container" class="wpforms-field wpforms-field-phone my-4" data-field-id="5"><label class="d-none wpforms-field-label wpforms-label-hide" for="wpforms-605-field_5">Phone <span class="wpforms-required-label">*</span></label><input type="tel" id="wpforms-605-field_5" class="wpforms-field-large wpforms-field-required form-control wpforms-smart-phone-field" data-rule-smart-phone-field="true" name="wpforms[fields][5]" placeholder="رقم الهاتف" required=""></div>
                            <div id="wpforms-605-field_4-container" class="wpforms-field wpforms-field-textarea my-4" data-field-id="4"><label class="d-none wpforms-field-label wpforms-label-hide" for="wpforms-605-field_4">Message <span class="wpforms-required-label">*</span></label><textarea id="wpforms-605-field_4" class="wpforms-field-medium wpforms-field-required form-control" cols="2" name="wpforms[fields][4]" placeholder="الرسالة" required=""></textarea></div>
                        </div>
                        <div class="wpforms-field wpforms-field-hp d-none"><label for="wpforms-605-field-hp" class="wpforms-field-label d-none">الرسالة</label><input type="text" name="wpforms[hp]" id="wpforms-605-field-hp" class="wpforms-field-medium"></div>
                        <div class="wpforms-submit-container"><input type="hidden" name="wpforms[id]" value="605"><input type="hidden" name="wpforms[author]" value="0"><input type="hidden" name="wpforms[post_id]" value="575"><button type="submit" name="wpforms[submit]" class="wpforms-submit btn btn-primary btn-block txtMons py-3 px-5" id="wpforms-submit-605" value="wpforms-submit" aria-live="assertive" data-alt-text="Sending..." data-submit-text="Submit">إرسال</button></div>
                    </form>
                </div>

                <!-- <form action="#" class="form pt-4"> -->
                <!-- <form id="wpforms-form-605" class="wpforms-validate wpforms-form" data-formid="605" method="post" enctype="multipart/form-data" action="/faris-wordpress/contact-us/"><noscript class="wpforms-error-noscript">Please enable JavaScript in your browser to complete this form.</noscript>
                    <div class="wpforms-submit-container"><input type="hidden" name="wpforms[id]" value="605"><input type="hidden" name="wpforms[author]" value="0"><input type="hidden" name="wpforms[post_id]" value="575"><button type="submit" name="wpforms[submit]" class="wpforms-submit " id="wpforms-submit-605" value="wpforms-submit" aria-live="assertive" data-alt-text="Sending..." data-submit-text="Submit">Submit</button></div>

                    <input required type="text" placeholder="Name" class="form-control" name="wpforms[fields][1]">
                    <input required type="Email" placeholder="Email" class="form-control my-3" name="wpforms[fields][2]">
                    <input required type="tel" placeholder="phone" class="form-control" name="wpforms[fields][3]">
                    <textarea required placeholder="Message" class="form-control my-3" name="wpforms[fields][4]"></textarea>
                    <div class="wpforms-field wpforms-field-hp"><label for="wpforms-605-field-hp" class="wpforms-field-label">Comment</label><input type="text" name="wpforms[hp]" id="wpforms-605-field-hp" class="wpforms-field-medium"></div>
                    <button class="btn btn-primary txtMons py-3 px-5">Send Message</button>
                </form> -->
                <!-- </form> -->

            </div>
        </div>
        <hr>
        <div class="mapouter">
            <div class="gmap_canvas">
                <?= get_field('map'); ?>
                <!-- <iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?ll=30.04245390000001,31.2002801&q=Mohi Al Din Abou Al Ezz&t=&z=14&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe> -->
            </div>

            <style>
                .mapouter {
                    position: relative;
                    text-align: right;
                    height: auto;
                    max-width: 100%;
                }

                .gmap_canvas {
                    overflow: hidden;
                    background: none !important;
                    height: auto;
                    -maxwidth: 100%
                }
            </style>

        </div>
        <hr>
    </div>
</div>




<?php get_footer() ?>