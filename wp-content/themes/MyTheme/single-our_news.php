<?php get_header() ?>


<div class="hero heroF" style="background: url('<?= get_field('section_1')['banner'] ?>') top center;">
    <div class="container">
        <div class="contentSection">
            <h2><?= get_field('section_1')["headline"]; ?></h2>
            <h1><?= get_field('section_1')["main_headline"]; ?></h1>
            <h4 class="pt-3"><?= get_field('section_1')["headline_description"]; ?></h4>
        </div>
    </div>
</div>


<div class="container tretms my-5">
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            // echo '<pre>';
            // var_export(the_author());
            // die;
    ?>
            <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <div class="post-inner">
                    <div class="entry-content">
                        <h3 class="txtLibre font-weight-bold colorB"><?= the_title(); ?></h3>
                        <p class="text-muted txtMons"><?= the_date() ?> / By <span class="text-muted"> <?= get_the_author() ?> </span></p>
                        <img src="<?= get_the_post_thumbnail_url($post->ID) ?>" class="imgCardNews mb-5" width="100%">
                        <!-- <h4 class="txtLibre colorB">What is new in this website that help you so much.</h4> -->
                        <!-- <h6 class="txtLibre colorB pt-2"> </h6> -->
                        <!-- <p class="pMe pt-2">Once you know you're pregnant you can book a visit at Dr. Faris Medical Center to track the progress of your growing bundle of joy. On your first visit our staff will compile your medical records containing all of your relevant history and data, and they we will introduce you to one of our highly qualified obstetricians who will be responsible for tracking your progress and making sure your baby is doing okay.</p>
                        <img src="<?= get_template_directory_uri() ?>/images/rectangle.png" class="img-fluid" alt="">
                        <p class="pMe pt-3">Once you know you're pregnant you can book a visit at Dr. Faris Medical Center to track the progress of your growing bundle of joy. On your first visit our staff will compile your medical records containing all of your relevant history and data, and they we will introduce you to one of our highly qualified obstetricians who will be responsible for tracking your progress and making sure your baby is doing okay.</p>
                        <p class="pMe pt-2">Once you know you're pregnant you can book a visit at Dr. Faris Medical Center to track the progress of your growing bundle of joy. On your first visit our staff will compile your medical records containing all of your relevant history and data, and they we will introduce you to one of our highly qualified obstetricians who will be responsible for tracking your progress and making sure your baby is doing okay.</p>
                        <p class="pMe pt-2">Once you know you're pregnant you can book a visit at Dr. Faris Medical Center to track the progress of your growing bundle of joy. On your first visit our staff will compile your medical records containing all of your relevant history and data, and they we will introduce you to one of our highly qualified obstetricians who will be responsible for tracking your progress and making sure your baby is doing okay.</p>
                     -->
                        <?php the_content() ?>
                    </div><!-- .entry-content -->
                </div><!-- .post-inner -->
            </article><!-- .post -->
    <?php
        }
    }
    ?>
</div>


<?php get_footer() ?>